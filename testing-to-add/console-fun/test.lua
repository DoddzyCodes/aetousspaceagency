local term   = require 'term'
local colors = term.colors -- or require 'term.colors'
local pretty = require 'pl.pretty'

local clock = os.clock
local function sleep(n)
    local t0 = clock()
    while clock() - t0 <= n do end
end

local function loadBar(y, x, segments)
    local bar = {};
    bar.drawValue = 0
    bar.value = 0
    bar.location = y or 1
    bar.init = false

    bar.segCount = segments or 50
    bar.posOffset = x or 0

    bar.width = 4 + bar.segCount + 7
    bar.height = 1

    bar.lineColor = colors.bright .. colors.blue

    bar.set = function(self, v)
        if v < 0 then v = 0 end
        if v > 100 then v = 100 end
        self.value = v
    end
    
    bar.draw = function(self)
        if not self.init then
            term.cursor.save()
            term.cursor.jump(self.location, self.posOffset)
            print("[ " .. string.rep(" ", self.segCount) ..  colors.reset .. "] " .. "( " .. self.drawValue .. "%)")
            term.cursor.restore()
            self.init = true
        elseif self.value ~= self.drawValue then
            term.cursor.save()
            
            local currX = self.posOffset + 2 + math.floor((self.drawValue / 100) * self.segCount)
            local newX =  self.posOffset + 2 + math.floor((self.value / 100) * self.segCount)

            if newX > currX then
                term.cursor.jump(self.location, currX)
                print(self.lineColor  .. string.rep("|", newX - currX) .. colors.reset)
            elseif newX < currX then
                term.cursor.jump(self.location, newX)
                print(string.rep(" ", currX - newX))
            end


            term.cursor.jump(self.location, self.posOffset + self.segCount + 4)
            
            --term.cleareol()
            print("( " .. self.value .. "%" .. string.rep(" ", 3 - string.len(self.value)) .. ")")

            term.cursor.restore()
            self.drawValue = self.value;

        end
    end

    return bar, y + bar.height, x + bar.width
end


local function loadGraph(width, height)
    local function replace_char(pos, str, r)
        return str:sub(1, pos-1) .. r .. str:sub(pos+1)
    end

    local first = true
    
    local g = {}
    g.values = {}
    g.width = width
    g.height = height

    g.addValue = function(self, x, y, value)
        local actualY = self.height - y + 1;
        if not self.values[actualY] then self.values[actualY] = {} end
        self.values[actualY][x + 1] = value
    end

    g.update = function(self)
         if not first then
            term.cursor.save()
            term.cursor.goup(self.height + 1)
            term.cleareol()
        end
        
        for i = 1, self.height do
            local str = "|" .. string.rep(".", self.width)

            if self.values[i] then
                for x, v in pairs(self.values[i]) do
                    str = replace_char(x, str, v)
                end
            end

            print(str)
        end

        local str = "|" .. string.rep("_", self.width)

        if self.values[self.height + 1] then
            for x, v in pairs(self.values[self.height + 1]) do
                str = replace_char(x, str, v)
            end
        end

        print(str)
        
        if not first then
            term.cursor.restore();
        else
            first = false
        end
    end

    return g;
end


local barWidth = 5
local barLength = 40
local y = 1
local x = 1
local bars = {}

local rowY = y
for c = 1, barLength do
    for r = 1, barWidth do
        local bar, newY, newX = loadBar(y, x, 20)
        x = newX + 3
        --x = newX
        local c = math.random(1, 4)
        if c == 1 then
            bar.lineColor = colors.bright .. colors.blue
        elseif c == 2 then
            bar.lineColor = colors.bright .. colors.red
        elseif c == 3 then
            bar.lineColor = colors.bright .. colors.green
        elseif c == 4 then
            bar.lineColor = colors.bright .. colors.magenta
        end

        currentLine = l
        bars[#bars + 1] = bar
        print(bar.location)
    end
    y = y + 1
    x = 1
end

term.clear()
local t = clock();
while true do
    for i = 1, #bars do
        bars[i]:set(math.random(1, 100))
        bars[i]:draw()
    end

    term.cursor.jump(y, 1)
    term.cleareol()
    
    local t0 = clock();
    print("RenderTime = " .. (t0 - t))
    t = t0

    sleep(0.1)
end

--[[
local graph = loadGraph(100, 30)
for i = 0, 30 do
    graph:addValue(i * 1.5,i, colors.red .. '*' .. colors.reset)
    graph:update()
    sleep(0.1)
end
]]

--graph:update()
