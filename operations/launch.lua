local configLoader = require 'pl.config'
local pretty = require 'pl.pretty'
local utils = require 'utilities'
local smath = require 'spacemath'
local speech = require 'speech'

local config = {}

--These will be assigned to correct values on launch initialization
local conn;
local platform;
local krpc;
local vessel;

--Global variables to be used and set during launch sequence
local timer = {
    sinceLaunch = 0,
    untilLaunch = -1,
    deltaTime = 0,
    launchTime = 0
}

local launchData = {
    autoStage = false,
    autoStageAutoDisable = false
}

local states = {
    INVALID = -1,
    PRELAUNCH = 0,
    VERTICAL_ASCENT = 1,
    MAIN_ASCENT = 2,
    COAST_TO_APOAPSIS = 3,
    CIRCULARIZATION_BURN = 4,
    COMPLETE =5
}


local state = PRELAUNCH

local function output(txt, speakLine, block)
    print(txt)
    if speakLine then speech.say(txt, block) end
end

local function getAutoStageData()
    if vessel.resources:amount('SolidFuel') > 0.1 then
        print("has solid fuel")
        launchData.autoStageHasSolidFuel = true
    else
        launchData.autoStageHasSolidFuel = false
    end


    if vessel.resources:amount('LiquidFuel') > 0.01 then
        print("has liquid fuel")
        launchData.autoStageHasLiquidFuel = true
    else
        launchData.autoStageHasLiquidFuel = false
    end
end

local function enableAutoStage(oneStageOnly)
    oneStageOnly = oneStageOnly or false

    launchData.autoStage = true
    launchData.autoStageAutoDisable = oneStageOnly
    getAutoStageData()
end

local function runAutoStage()
    if not launchData.autoStage or state == states.INVALID or state == states.COMPLETE 
    or state == states.PRELAUNCH or state == states.COAST_TO_APOAPSIS or state == states.CIRCULARIZATION_BURN then return end

    if (launchData.autoStageHasSolidFuel and vessel.resources:amount('SolidFuel') < 0.1) or vessel.thrust == 0 then
        local currentSASMode = vessel.auto_pilot.sas_mode
        local SASEnabled = vessel.auto_pilot.sas
        
        vessel.auto_pilot.sas_mode = conn.space_center.SASMode.stability_assist
        vessel.auto_pilot.sas = true
        
        vessel.control:activate_next_stage()
        utils.sleep(1)
        
        vessel.auto_pilot.sas_mode = currentSASMode
        vessel.auto_pilot.sas = vessel.auto_pilot.sas

        getAutoStageData()
    end

    if launchData.autoStageAutoDisable then launchData.autoStage = false end
end


--Variables used for prelaunch
local announceTime = 0
local throttleSet = false;
local function runPrelaunch()
    --Called first prelaunch frame
    if timer.untilLaunch == -1 then
        timer.untilLaunch = config.launch_countdown_time or 3
        announceTime = math.ceil(timer.untilLaunch) + 1

        vessel.control.throttle = 0.0

        output("Prelaunch checks complete, Launch countdown initiated.", true, true)
        vessel.auto_pilot:target_pitch_and_heading(90, 90)
        vessel.auto_pilot:engage()

        utils.sleep(1.0)
    else
        timer.untilLaunch = timer.untilLaunch - timer.deltaTime

        --Coundown
        if math.ceil(timer.untilLaunch) < announceTime then
            announceTime = math.ceil(timer.untilLaunch)
            output(announceTime, true, false)
        end

        if timer.untilLaunch < 2 and not throttleSet then
            vessel.control.throttle = 1.0
            throttleSet = true
            output("Thottle set", true, false)
        end


        --LAUNCH!
        if timer.untilLaunch < 0 then
            vessel.control:activate_next_stage()
            state = states.VERTICAL_ASCENT
            output("We have Ignition", true, false)
        end
    end
end

local announcedClearOfTower = false
local function runVerticalAscent()
    local targetAscentSpeed = config.target_ascent_speed or 100

    local obtFrame = vessel.orbit.body.non_rotating_reference_frame
    local srfFrame = vessel.orbit.body.reference_frame

    local currentSpeed = vessel:flight(srfFrame).speed
    local currentAltitude = vessel:flight(srfFrame).mean_altitude
    --output(currentSpeed .. " " .. currentAltitude, false)
    if currentAltitude > 100 and not announcedClearOfTower then
        output("Vehicle has cleared the tower", true, false)
        announcedClearOfTower = true
    end

    if currentSpeed >= targetAscentSpeed then
        state = states.MAIN_ASCENT
        output("Begining gravity turn", true, false)
    end
end

local function runMainAscent()
    local maxLowAltitudeSpeed = config.max_low_altitude_speed or 500
    local allowedSpeedVariance = config.allowed_speed_variance or 0.1
    local lowAltitudeEnds = config.low_altitude_ends or 15000
    local gravityTurnStartSpeed = config.target_ascent_speed or 50

    local orbitAltitude = config.oribit_altitude or 80000
    
    local targetPitch = config.target_pitch or 90
    local rotatePower = config.rotate_power or 3

    local obtFrame = vessel.orbit.body.non_rotating_reference_frame
    local srfFrame = vessel.orbit.body.reference_frame

    local currentSpeed = vessel:flight(srfFrame).speed
    local currentAltitude = vessel:flight(srfFrame).mean_altitude

    if currentAltitude < lowAltitudeEnds then
        local suggestedSpeed = (currentAltitude / lowAltitudeEnds) * (maxLowAltitudeSpeed - gravityTurnStartSpeed) + gravityTurnStartSpeed
        local minAllowedSpeed = suggestedSpeed - (suggestedSpeed * allowedSpeedVariance)
        local maxAllowedSpeed = suggestedSpeed + (suggestedSpeed * allowedSpeedVariance)
        local throttleSetting = 1.0 - smath.reverseLerp(minAllowedSpeed, maxAllowedSpeed, currentSpeed)
        vessel.control.throttle = math.max(0.3, throttleSetting)
    else
        vessel.control.throttle = 1
    end
    --abs($E$6*pow(A2-$E$2, $G$2))
    local a = targetPitch / math.pow(orbitAltitude, rotatePower)
    local suggestedPitch = math.abs(a * math.pow(currentAltitude - orbitAltitude, rotatePower))
    vessel.auto_pilot.target_pitch = suggestedPitch

    local currentApoapsisAltitude = vessel.orbit.apoapsis_altitude
    if (currentApoapsisAltitude >= orbitAltitude) then
        vessel.control.throttle = 0
        state = states.COAST_TO_APOAPSIS
        output("Vehicle is now on coast to Apo-apsis", true, false)
    end

    --print("Suggested Speed: " .. suggestedSpeed .. ", Suggested Pitch: " .. suggestedPitch .. ", T: " .. vessel.control.throttle .. ", P: " .. vessel.auto_pilot.target_pitch)
end
local function calculateCircularizationDeltaV()
    local gm = 3531000000000
    local br = vessel.orbit.body.equatorial_radius

    local orbitHeight = vessel.orbit.apoapsis_altitude
    local currentPeriapsis = vessel.orbit.periapsis_altitude

    local r0 = br + orbitHeight
    local r1 = r0

    local a0 = 2*br + orbitHeight + currentPeriapsis
    local a1 = 2*br + 2 * orbitHeight

    local v0 = math.sqrt(gm * ((2 / r0) - (2 / a0)))
    local v1 = math.sqrt(gm * ((2 / r1) - (2 / a1)))

    return v1 - v0
end

local function circularizationPitchCalculation()
    local orbitAltitude = config.oribit_altitude or 80000
    
    local maxAllowedCircularizationPitch = config.max_allowed_circularization_pitch or 15
    local maxAllowedApoapsisOffset = config.max_allowed_apoapsis_offset or 100

    local currentApoHeight = vessel.orbit.apoapsis_altitude


    local minAlt = orbitAltitude - maxAllowedApoapsisOffset;
    local maxAlt = orbitAltitude + maxAllowedApoapsisOffset;
    local t = smath.reverseLerp(minAlt, maxAlt, currentApoHeight)
    local targetPitch = maxAllowedCircularizationPitch - t * (2 * maxAllowedCircularizationPitch)
    return targetPitch
end

local function runCoastToApoapsis()
    
    local requiredDV = calculateCircularizationDeltaV()
    if vessel.orbit.time_to_apoapsis > 30 then
        vessel.control.throttle = 0
        vessel.auto_pilot.set_pitch = circularizationPitchCalculation()
    else
        state = states.CIRCULARIZATION_BURN
        vessel.control.throttle = 0.0
        output("Performing circularization burn", true, false)
    end
end

local function runCircularizationBurn()
    local orbitAltitude = config.oribit_altitude or 80000
    
    vessel.auto_pilot.target_pitch = circularizationPitchCalculation();

    local timeToApo = vessel.orbit.time_to_apoapsis

    local throttle = 1
    if timeToApo < 40 then

        local targetTimeToApo = 30 - (math.max(0, vessel.orbit.periapsis_altitude / orbitAltitude) * 30)

        throttle = 1 - smath.reverseLerp(targetTimeToApo - 3, targetTimeToApo + 3, timeToApo)
    end
    vessel.control.throttle = throttle
    
    if(vessel.orbit.periapsis_altitude > orbitAltitude) then
        state = states.COMPLETE
        vessel.control.throttle = 0
        vessel.auto_pilot:disengage()
        output("Vehicle is in orbit!", true, false)
    end
end

local function runLaunch()
    timer.deltaTime = config.update_speed or 0.1
    timer.launchTime = os.clock()
    timer.deltaTime = 0.1
    while true do
        timer.sinceLaunch = timer.launchTime - os.clock();

        if state == states.INVALID then
            output("Flight entered invalid state, cancelling launch!")
            return false
        elseif state == states.COMPLETE then
            output("Launch complete, have a good flight!", true, false)
            return true
        elseif state == states.PRELAUNCH then
            runPrelaunch()
        elseif state == states.VERTICAL_ASCENT then
            runVerticalAscent()
        elseif state == states.MAIN_ASCENT then
            runMainAscent()
        elseif state == states.COAST_TO_APOAPSIS then
            runCoastToApoapsis()
        elseif state == states.CIRCULARIZATION_BURN then
            runCircularizationBurn()
        end

        runAutoStage()
        utils.sleep(timer.deltaTime)
    end
end

return function(commandArgs)
    krpc = require 'krpc'
    platform = require 'krpc.platform'
    conn = krpc.connect('Sub-orbital flight')
    vessel = conn.space_center.active_vessel

    config = configLoader.read(commandArgs[1] or "launch.config")

    enableAutoStage()

    state = states.PRELAUNCH

    runLaunch()
end