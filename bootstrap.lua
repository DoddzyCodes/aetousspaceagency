

--Need to automate this next bit..
package.path = ".\\?.lua;.\\libs\\?.lua;" .. package.path;
package.cpath = ".\\libs\\?.dll;" .. package.cpath;
--Check for required modules
--local lfs = require("lfs");

local utils = require("utilities")



local function startupCheck()
    if (not utils.isModuleAvailable("lfs")) then
        print("Unable to find required module 'lfs'.  Please install before running Flight Bootstrap!")
        return false;
    end
    if (not utils.isModuleAvailable("krpc")) then
            print("Unable to find required module 'krpc'.  Please install before running Flight Bootstrap!")
        return false;
    end

    return true;
end


print("Running Bootstrap startup...")
if (not startupCheck()) then
    print("Bootstrap initialization failed!")
    return
else
    print("Bootstrap initialization succeeded!")
end

local commands = {}
commands.launch = require("operations.launch");

if not commands[arg[1]] then
    print("Invalid System command, exiting!")
    return
else
    local commandArgs = utils.shallowCopy(arg);
    table.remove(commandArgs, 1);
    commands[arg[1]](commandArgs)
end