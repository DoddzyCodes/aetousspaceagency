local speechPath = ".\\bin\\eSpeak\\"

local function say(txt, block)
    local cmd =  speechPath .. 'espeak -ven+m2 -p70 -s170 "' .. txt .. '"'
    if not block then
        assert(io.popen(cmd))
    else
        os.execute(cmd)
    end
end

return {
    say = say
}