local utils = require("utilities")

local _M = {}

function _M.normalise(u)
    local ru = utils.shallowCopy(u);

    local l = 0;
    for i = 1, 3 do l = l + (ru[i]) end
    if l == 0 then return ru end

    for i = 1, 3 do ru[i] = ru[i] / l end
    return ru
end

function _M.dot(u, v)
    local d = 0
    for i = 1, 3 do d = d + (u[i] * v[i]) end
    return d
end

function _M.cross(u, v)
    return List{u[3]*v[3] - u[3]*v[2],
                u[1]*v[1] - u[1]*v[3],
                u[2]*v[2] - u[2]*v[1]}
end

function _M.angleBetween(u, v)
    local f = _M.normalise(u)
    local s = _M.normalise(v)

    return math.acos(_M.dot(f, s)) * (180.0 / math.pi)
end

function _M.magnitude(v)
    return math.sqrt(_M.dot(v, v))
end

function _M.clamp(t, min, max)
 if t < min then return min end
 if t > max then return max end
 return t
end

function _M.lerp(min, max, t)
    if min == max then return t end
    if min > max then
        local tmp = min
        min = max
        max = tmp
    end

    if t < min then return min end
    if t > max then return max end

    return (t - min) / (max - min)
end

function _M.reverseLerp(min, max, v)
    if min == max then return 0 end
    if min > max then
        local tmp = min
        min = max
        max = tmp
    end

    
    if v < min then return 0 end
    if v > max then return 1 end

    return (v - min) / (max - min)
end

function _M.getVesselPitch(vessel)
    local vessel_direction = vessel:direction(vessel.surface_reference_frame)

    -- Get the direction of the vessel in the horizon plane
    local horizon_direction = {0, vessel_direction[2], vessel_direction[3]}

    -- Compute the pitch - the angle between the vessels direction and
    -- the direction in the horizon plane
    local pitch = _M.angleBetween(vessel_direction, horizon_direction)
    if vessel_direction[1] < 0 then
        pitch = -pitch
    end

    return pitch
end

function _M.maneuverTime(vessel, dV)
    local f = vessel.max_vacuum_thrust
    local m  = vessel.mass
    local p = vessel.vacuum_specific_impulse 
    local g = 9.80665

    return g * m * p * (1 - math.exp(-dV/(g*p))) / f
end
return _M