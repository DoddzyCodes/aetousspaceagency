local _M = {}

function _M.isModuleAvailable(name)
  if package.loaded[name] then
    return true
  else
    for _, searcher in ipairs(package.searchers or package.loaders) do
      local loader = searcher(name)
      if type(loader) == 'function' then
        package.preload[name] = loader
        return true
      end
    end
    return false
  end
end

function _M.shallowCopy(t)
  if type(t) ~= "table" then return t
  else
    local rt = {}
    for k, v in pairs(t) do
      rt[k] = v
    end

    return rt
  end
end


function _M.checkUserauthorization(msg)
  print(msg)
  local ready = io.read();
  return ready == "y" or ready == "yes"
end

local clock = os.clock
function _M.sleep(n)  -- seconds
  local t0 = clock()
  while clock() - t0 <= n do end
end

return _M